<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>Login</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/bootstrap.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/style.css" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.validate.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/validateSubmitForms.js"></script>
  
</head>

<body>


<div class="head">
  <img src="${pageContext.request.contextPath}/client/img/surf-oauth.png"/>
</div>


<div class="main" id="loginFormDiv">
  <div class="full">
    <div class="page-header">
      <h1>Login</h1>
    </div>
	<p id="loginErrorMessage" style="visible:hidden; color: red; font: bold;">
	</p>
    <form class="form-horizontal" id="loginForm" method="post"
      action="${actionUri}">
      <fieldset>
        <div class="control-group">
          <label class="control-label">Identifier</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="username"
              name="j_username" rel="popover"
              data-content="Enter your identifier."
              data-original-title="Identifier" />
          </div>
        </div>

        <div class="control-group">
          <label class="control-label">Password</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="password"
              name="j_password" rel="popover"
              data-content="What's your password?"
              data-original-title="Password" />
          </div>
        </div>
        <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
        <input type="hidden" name="oldProfileId" value="${oldProfileId}" />
      </fieldset>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Login</button>
      </div>
    </form>
  </div>
</div>

<div class="main" id="forgorPasswordFormDiv">
  <div class="full">
    <div class="page-header">
      <h1>Forgot password</h1>
    </div>
	<p id="forgorPasswordResultMessage" style="visible:hidden; color: red; font: bold;">
	</p>
    <form class="form-horizontal" id="forgorPasswordForm" method="post" action="${actionUri}">
      <input type="hidden" name="action" value="forgot_password"/>
      <fieldset>
        <div class="control-group">
          <label class="control-label">E-mail:</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="email"
              name="email" />
          </div>
        </div>
        <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
      </fieldset>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary">E-mail my password</button>
      </div>
    </form>
  </div>
</div>

<div class="main" id="registerFormDiv">
  <div class="full">
    <div class="page-header">
      <h1>Register Account</h1>
    </div>
	<p id="registerErrorMessage" style="visible:hidden; color: red; font: bold;">
	</p>
    <form class="form-horizontal" id="registerForm" method="post"
      action="${actionUri}">
      <input type="hidden" name="action" value="register"/>
       
      <fieldset>
      
        <div class="control-group">
          <label class="control-label">Title</label>
          <div class="controls">
          	  <select name="j_prefix">
          	  		<option value="Mr">Mr</option>
          	  		<option value="Mrs">Mrs</option>
          	  		<option value="Ms">Ms</option>
          	  </select>
          </div>
        </div>
      
      	<div class="control-group">
          <label class="control-label">First Name</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="firstName"
              name="j_firstName" rel="popover"
              data-content="Enter First Name."
              data-original-title="FirstName" />
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Last Name</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="lastName"
              name="j_lastName" rel="popover"
              data-content="Enter Last Name."
              data-original-title="LastName" />
          </div>
        </div>
        
      	<div class="control-group">
          <label class="control-label">Login</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="login"
              name="j_username" rel="popover"
              data-content="Enter Login."
              data-original-title="Login" />
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Password</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="j_password"
              name="j_password" rel="popover"
              data-content="Enter old password."
              data-original-title="Old Password" />
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Confirm New Password</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="j_confirmPassword"
              name="j_confirmPassword" rel="popover"
              data-content="Confirm New Password"
              data-original-title="Confirm New Password" />
          </div>
        </div>
        <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
      </fieldset>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Register</button>
        <button type="submit" class="btn btn-primary">Cancel</button>
      </div>
    </form>
  </div>
</div>

<div class="foot">
  <p>Powered by <a href="http://www.surfnet.nl/">SURFnet</a>. Fork me on <a href="https://github.com/OpenConextApps/oa-aas/">Github</a>. Licensed under the <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License 2.0</a>.</p>
</div>

</body>
</html>