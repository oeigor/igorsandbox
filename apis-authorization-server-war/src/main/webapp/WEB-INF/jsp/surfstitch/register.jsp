<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <title>Register</title>
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/bootstrap.min.css" />
  <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/style.css" />
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/jquery.js"></script>
  <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/bootstrap.min.js"></script>
</head>

<body>

<div class="head">
  <img src="${pageContext.request.contextPath}/client/img/surf-oauth.png"/>
</div>

<div class="main">
  <div class="full">
    <div class="page-header">
      <h1>Register Account</h1>
    </div>

    <form class="form-horizontal" id="registerHere" method="post"
      action="${actionUri}">
      <input type="hidden" name="action" value="register"/>
       
      <fieldset>
      
        <div class="control-group">
          <label class="control-label">Title</label>
          <div class="controls">
          	  <select name="j_prefix">
          	  		<option value="Mr">Mr</option>
          	  		<option value="Mrs">Mrs</option>
          	  		<option value="Ms">Ms</option>
          	  </select>
	          <p class="help-block">Hint: your title (Mr/Mrs/Ms)</p>
          </div>
        </div>
      
      	<div class="control-group">
          <label class="control-label">First Name</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="firstName"
              name="j_firstName" rel="popover"
              data-content="Enter First Name."
              data-original-title="FirstName" />
            <p class="help-block">Hint: your first name</p>
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Last Name</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="lastName"
              name="j_lastName" rel="popover"
              data-content="Enter Last Name."
              data-original-title="LastName" />
            <p class="help-block">Hint: your last name</p>
          </div>
        </div>
        
      	<div class="control-group">
          <label class="control-label">Login</label>
          <div class="controls">
            <input type="text" class="input-xlarge" id="login"
              name="j_username" rel="popover"
              data-content="Enter Login."
              data-original-title="Login" />
            <p class="help-block">Hint: your username</p>
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Password</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="oldPassword"
              name="j_password" rel="popover"
              data-content="Enter old password."
              data-original-title="Old Password" />
            <p class="help-block">Hint: think hard</p>
          </div>
        </div>
        
        <div class="control-group">
          <label class="control-label">Confirm New Password</label>
          <div class="controls">
            <input type="password" class="input-xlarge" id="confirmNewPassword"
              name="j_confirmPassword" rel="popover"
              data-content="Confirm New Password"
              data-original-title="Confirm New Password" />
            <p class="help-block">Hint: confirm thinking smart</p>
          </div>
        </div>
        <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
        <input type="hidden" name="client_id" value="${param.client_id}" />
      </fieldset>

      <div class="form-actions">
        <button type="submit" class="btn btn-primary">Register</button>
        <button type="submit" class="btn btn-primary">Cancel</button>
      </div>
    </form>
  </div>
</div>

<div class="foot">
  <p>Powered by <a href="http://www.surfnet.nl/">SURFnet</a>. Fork me on <a href="https://github.com/OpenConextApps/oa-aas/">Github</a>. Licensed under the <a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License 2.0</a>.</p>
</div>

</body>
</html>