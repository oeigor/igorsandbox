<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Login</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/fonts/fontello/css/fontello-embedded.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/style.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap-select/bootstrap-select.min.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap-select/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/validateSubmitForms.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/dynamicUrlsHandler.js"></script>
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));

        ga('create', 'UA-5640694-1', 'auto');
        ga('create', 'UA-5640694-2', 'auto', {'name': 'secondTracker'});
        ga('create', 'UA-25894222-1', 'auto', {'name': 'thirdTracker'});

        ga('require', 'ec');
        ga('secondTracker.require', 'ec');
        ga('thirdTracker.require', 'ec');

        ga('ec:setAction', 'checkout', {'step':2});
        ga('secondTracker.ec:setAction', 'checkout', {'step':2});
        ga('thirdTracker.ec:setAction', 'checkout', {'step':2});

        ga('send', 'pageview');       // Send product details view with the initial pageview.
        ga('secondTracker.send', 'pageview');
        ga('thirdTracker.send', 'pageview');
    </script>

</head>

<body>


<div class="head">
    <a id="back" href="#back"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
    <a id="siteUrl" href="#siteUrl"><img  id="logo" src="${pageContext.request.contextPath}/client/img/logo.png"/></a>
    <a id="close" href="#close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
</div>

<div class="sub-head">
    <h1>My Account Login</h1>
</div>


<div class="full container-fluid">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
                <div class="panel panel-surfstitch">
                    <div id="returningCustomer" class="panel-heading" role="tab" id="headingOne">
                        <h2 class="panel-title">
                            <a id="returningCustomerLink"  class="collapsed"  data-toggle="collapse" data-parent="#accordion" href="#returningCustomerContent" aria-expanded="false" aria-controls="returningCustomerContent">
                                <span class="main-title">Sign In</span>
                                <span class="sub-title">For returning customers.</span>
                                <span class="fontello icon-cw" aria-hidden="true"></span>
                            </a>

                        </h2>
                    </div>
                    <div id="returningCustomerContent" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <div class="main" id="loginFormDiv">
                                <p id="loginErrorMessage" style="visible:hidden; color: red; font: bold;">
                                </p>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-9 col-md-9">
                                        <form id="loginForm" method="post" action="${actionUri}" role="form">

                                            <div class="form-group">
                                                <label class="sr-only">Email*</label>
                                                <input type="email" class="form-control input-lg" id="username" name="j_username" rel="popover" data-content="Enter your identifier." data-original-title="Identifier" autocapitalize="none" autocomplete="on" autocorrect="off" placeholder="Email*"/>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only">Password*</label>
                                                <div class="controls">
                                                    <input type="password" class="form-control input-lg" id="password" name="j_password" rel="popover" data-content="What's your password?" data-original-title="Password" autocapitalize="none" autocomplete="off" autocorrect="off" placeholder="Password*"/>
                                                </div>
                                            </div>
                                            <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
                                            <input type="hidden" name="old_profile_id" value="${old_profile_id}" />
                                            <input type="hidden" name="client_id" value="${param.client_id}" />
                                            <!--<p class="help-block">*Required</p>-->
                                            <button type="submit" class="sr-only">Login</button>
                                        </form>
                                    </div>

                                    <div class="col-xs-12 col-sm-3 col-md-3">
                                        <button id="loginButton-1" type="submit" class="btn-lg btn-block btn-default" data-state="login_call">Go</button>
                                    </div>
                                </div>
                            </div>

                            <div class="main hidden" id="forgorPasswordFormDiv">
                                <p id="forgorPasswordResultMessage" style="visible:hidden; color: red; font: bold;">
                                </p>
                                <form id="forgorPasswordForm" method="post" action="${actionUri}" role="form">
                                    <input type="hidden" name="action" value="forgot_password"/>
                                    <div class="form-group">
                                        <label class="sr-only">Your Email*</label>
                                        <div class="controls">
                                            <input type="email" class="form-control input-lg" id="email" name="email" autocapitalize="none" autocomplete="off" autocorrect="off" placeholder="Your Email*"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
                                    <!--<p class="help-block">*Required</p>-->
                                    <button type="submit" class="sr-only">E-mail my password</button>
                                </form>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button id="loginButton-2" type="submit" class="btn-lg btn-block btn-default" data-state="login_call">Login</button>
                                </div>
                                <div class="col-xs-12">
                                    <button id="forgotPasswordButton" type="submit" class="btn-lg btn-block btn-default" data-state="forgot_password_invoke">Forgot your password?</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="newCustomer" class="panel panel-surfstitch">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h2 class="panel-title">
                            <a id="newCustomerLink" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#newCustomerContent" aria-expanded="false" aria-controls="newCustomerContent">
                                <span class="main-title">Sign Up</span>
                                <span class="sub-title state-closed">New Customer?</span>
                                <span class="sub-title state-open">Save your details, reap the benefits</span>
                                <span class="fontello icon-certificate" aria-hidden="true"></span>
                            </a>
                    <span class="header-hero">
                        <span>Add your details for:</span>
                        <span>
                            -	faster checkout;<br/>
                            - 	order tracking; and<br/>
                            -	email updates & special offers.<br/>
                        </span>
                    </span>
                        </h2>

                    </div>
                    <div id="newCustomerContent" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <div class="main" id="registerFormDiv">
                                <p id="registerErrorMessage" style="visible:hidden; color: red; font: bold;">
                                </p>
                                <form id="registerForm" method="post" action="${actionUri}" role="form">

                                    <input type="hidden" name="action" value="register"/>

                                    <div class="form-group">
                                        <label class="sr-only">Title*</label>
                                        <div class="controls">
                                            <select name="j_prefix" class="form-control input-lg show-tick required" title='Please Select'>
                                                <option value="">Title*</option>
                                                <option value="Mr">Mr</option>
                                                <option value="Mrs">Mrs</option>
                                                <option value="Ms">Ms</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only">First Name*</label>
                                        <div class="controls">
                                            <input type="text" class="form-control input-lg" id="firstName" name="j_firstName" rel="popover" data-content="Enter First Name." data-original-title="FirstName" autocomplete="off" autocorrect="off" placeholder="First Name*"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only">Last Name*</label>
                                        <div class="controls">
                                            <input type="text" class="form-control input-lg" id="lastName" name="j_lastName" rel="popover" data-content="Enter Last Name." data-original-title="LastName" autocomplete="off" autocorrect="off" placeholder="Last Name*"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only">Email*</label>
                                        <div class="controls">
                                            <input type="email" class="form-control input-lg" id="login" name="j_username" rel="popover" data-content="Enter email." data-original-title="Email" autocomplete="off" autocorrect="off" placeholder="Email*"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only">Password*</label>
                                        <div class="controls">
                                            <input type="password" class="form-control input-lg" id="j_password" name="j_password" rel="popover" data-content="Enter old password." data-original-title="Old Password" autocomplete="off" autocorrect="off" placeholder="Password* (5-40 Characters)"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="sr-only">Confirm Password*</label>
                                        <div class="controls">
                                            <input type="password" class="form-control input-lg" id="j_confirmPassword" name="j_confirmPassword" rel="popover" data-content="Confirm New Password" data-original-title="Confirm New Password" autocomplete="off" autocorrect="off" placeholder="Password* (5-40 Characters)"/>
                                        </div>
                                    </div>
                                    <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
                                    <input type="hidden" name="old_profile_id" value="${old_profile_id}" />
                                    <input type="hidden" name="client_id" value="${param.client_id}" />
                                    <p class="help-block">
                                        By submitting your details, you agree to provide your personal information to improve our products and services, conduct surveys, process payments and understand you better. You also consent to being added to our email database so we can send you information and offers. You can easily unsubscribe via a link in our emails or by contacting CustomerService@Surfstitch.com. For more information, view our Privacy Policy.
                                    </p>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn-lg btn-block btn-primary">I'm Done!</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a id="guestCheckout" href="${param.redirect_uri}">
                <span class="main-title">Continue As A Guest</span>
                <span class="sub-title">Go straight through to checkout.</span>
                <span class="fontello icon-users" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>
<div class="foot">
    <div id="ssl-safe"></div>
</div>

</body>
</html>