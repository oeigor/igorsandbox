<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Change Password</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/bootstrap.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/css/3.0/style.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap-select/bootstrap-select.min.css" />
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/bootstrap-select/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/lib/3.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/validateSubmitForms.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/client/js/dynamicUrlsHandler.js"></script>
    <script>
        (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));

        ga('create', 'UA-5640694-1', 'auto');
        ga('create', 'UA-5640694-2', 'auto', {'name': 'secondTracker'});
        ga('create', 'UA-25894222-1', 'auto', {'name': 'thirdTracker'});

        ga('require', 'ec');
        ga('secondTracker.require', 'ec');
        ga('thirdTracker.require', 'ec');

        ga('ec:setAction', 'checkout', {'step':2});
        ga('secondTracker.ec:setAction', 'checkout', {'step':2});
        ga('thirdTracker.ec:setAction', 'checkout', {'step':2});

        ga('send', 'pageview');       // Send product details view with the initial pageview.
        ga('secondTracker.send', 'pageview');
        ga('thirdTracker.send', 'pageview');
    </script>


</head>

<body>


<div class="head">
    <a id="back" href="#back"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
    <a id="siteUrl" href="#siteUrl"><img  id="logo" src="${pageContext.request.contextPath}/client/img/logo.png"/></a>
    <a id="close" href="#close"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
</div>

<div class="sub-head">
    <h1>My Account</h1>
</div>


<p class="message">To change your password, please provide your current password and enter the new password below twice.</p>
<div class="full container-fluid">
    <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
        <div id="newCustomer" class="panel panel-surfstitch">
            <div class="panel-heading" role="tab" id="headingTwo">
                <h2 class="panel-title">
                    <a id="newCustomerLink" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#newCustomerContent" aria-expanded="false" aria-controls="newCustomerContent">
                        Change My Password
                        <span class="glyphicon" aria-hidden="true"></span>
                    </a>

                </h2>
            </div>
            <div id="newCustomerContent" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                    <div class="main" id="registerFormDiv">
                        <p id="registerErrorMessage" style="visible:hidden; color: red; font: bold;">
                        </p>
                        <form id="changePassword" method="post" action="${actionUri}" role="form">
                            <div class="form-group">
                                <label class="control-label">Login*</label>
                                <div class="controls">
                                    <input type="email" class="form-control input-lg" id="login"
                                           name="j_username" rel="popover"
                                           data-content="Enter Login."
                                           data-original-title="Login"
                                           autocomplete="off" autocorrect="off" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Old Password*</label>
                                <div class="controls">
                                    <input type="password" class="form-control input-lg" id="oldPassword"
                                           name="j_oldPassword" rel="popover"
                                           data-content="Enter old password."
                                           data-original-title="Old Password"
                                           autocomplete="off" autocorrect="off" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">New Password*</label>
                                <div class="controls">
                                    <input type="password" class="form-control input-lg" id="j_newPassword"
                                           name="j_newPassword" rel="popover"
                                           data-content="Enter new password."
                                           data-original-title="New Password"
                                           autocomplete="off" autocorrect="off" placeholder="(5-40 Characters)" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Confirm New Password*</label>
                                <div class="controls">
                                    <input type="password" class="form-control input-lg" id="j_confirmNewPassword"
                                           name="j_confirmNewPassword" rel="popover"
                                           data-content="Confirm New Password"
                                           data-original-title="Confirm New Password"
                                           autocomplete="off" autocorrect="off" placeholder="(5-40 Characters)" />
                                </div>
                            </div>
                            <input type="hidden" name="AUTH_STATE" value="${AUTH_STATE}" />
                            <input type="hidden" name="client_id" value="${param.client_id}" />
                            <p class="help-block">*Required</p>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-xs-6 col-xs-offset-3">
                                        <button type="submit" class="btn-lg btn-block btn-primary">Change Password</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="foot">
    <div id="ssl-safe"></div>
</div>

</body>
</html>