$(function() {
    'use strict';

    $.validator.addMethod("valueNotEquals", function(value, element, arg){
        return arg !== value;
    }, "Value must not equal arg.");

    var $forgotPasswordButton = $('#forgotPasswordButton'),
        $stateControlButton = $('#loginButton-2'),
        $loginButton = $('#loginButton-1'),
        state = "default",
        $returningCustomer = $('#returningCustomer'),
        $newCustomer = $('#newCustomer');

    // Login form validation
    $("#loginForm").validate({
        rules: {
            j_username: {
                required: true
            },
            j_password : {
                required: true,
                minlength: 5,
                maxlength: 40
            }
        },

        messages: {
            j_username: "Please enter username",
            j_password: {
                required: "Please enter password",
                minlength: "Password must be 5-40 characters",
                maxlength: "Password must be 5-40 characters"
            }
        },

        submitHandler: function(form) {
            var $form = $(this);
            var postData = $(form).serializeArray();
            $.ajax({
                type: "POST",
                url: "/oauth2/authorize",
                data: postData,
                success: function(data, textStatus, request) {
                    $("p#loginErrorMessage").hide();
                    // login is OK, just reload page - redirect to main page
                    window.location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                	if (jqXHR.status == 0) {
                		window.location.reload();
                		return;
                	}
                    // display error message
                    $("p#loginErrorMessage").text(jqXHR.statusText);
                    $("p#loginErrorMessage").show();
                }
            });
        }
    });

    // Forgot password form validation
    $("#forgorPasswordForm").validate({
        rules: {
            email: {
                required: true,
                email: true
            }
        },

        messages: {
            email: "Please enter a valid email address"
        },

        submitHandler: function(form) {
            var $form = $(this);
            var postData = $(form).serializeArray();
            $.ajax({
                type: "POST",
                url: "/oauth2/authorize",
                data: postData,
                success: function(data, textStatus, jqXHR) {
                  $("p#forgorPasswordResultMessage").hide();
                  window.location.href = jqXHR.getResponseHeader("redirect");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                	if (jqXHR.status == 0) {
                		$("p#forgorPasswordResultMessage").hide();
                		window.location.href = jqXHR.getResponseHeader("redirect");
                		return;
                	}
                    $("p#forgorPasswordResultMessage").text("Error during send temporary password to your email.");
                    $("p#forgorPasswordResultMessage").show();
                }
            });
        }
    });

    $.validator.addMethod("passwordPolicy", function(value, element) {
    	  return this.optional(element) || (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/).test(value); 
    	}, "Password must be 8 characters in length and must contain both upper-case and lower-case letters.<br />Password must not contain any special characters, symbols or spaces.");

    // Login form validation
    $("#registerForm").validate({
        ignore: ":not(select:hidden, input:visible, textarea:visible)",
        errorPlacement: function(error, element) {
            if(element.is('select:hidden')){
                element = element.siblings('.bootstrap-select');
            }
            element.after(error);
        },
        rules: {
            j_prefix: {
                required: true
            },
            j_firstName: {
                required: true
            },
            j_lastName : {
                required: true
            },
            j_username: {
                required: true
            },
            j_password: {
                required: true,
                minlength: 8,
                maxlength: 40,
                passwordPolicy: true
            },
            j_confirmPassword: {
                required: true,
                equalTo: "#j_password",
                minlength: 8,
                maxlength: 40
            }
        },

        messages: {
            j_prefix: "Please select a title" ,
            j_firstName: "Please enter first name",
            j_lastName: "Please enter last name",
            j_username: "Please enter email",
            j_password: {
                required: "Please enter password",
                minlength: "Password must be 5-40 characters",
                maxlength: "Password must be 5-40 characters"
            },
            j_confirmPassword: {
                required: "Please confirm password",
                equalTo: "Password fields have to match",
                minlength: "Password must be 5-40 characters",
                maxlength: "Password must be 5-40 characters"
            }
        },

        submitHandler: function(form) {
            var $form = $(this);
            var postData = $(form).serializeArray();
            $.ajax({
                type: "POST",
                url: "/oauth2/authorize",
                data: postData,
                success: function(data) {
                    $("p#registerErrorMessage").hide();
                    // login is OK, just reload page - redirect to main page
                    window.location.reload();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                	if (jqXHR.status == 0) {
                		window.location.reload();
                		return;
                	}
                    // display error message
                    $("p#registerErrorMessage").text(jqXHR.statusText);
                    $("p#registerErrorMessage").show();
                }
            });
        }
    });

    // Login form validation
    $("#changePassword").validate({
        ignore: ":not(select:hidden, input:visible, textarea:visible)",
        errorPlacement: function(error, element) {
            if(element.is('select:hidden')){
                element = element.siblings('.bootstrap-select');
            }
            element.after(error);
        },
        rules: {
            j_username: {
                required: true
            },
            j_oldPassword: {
                required: true,
                minlength: 5,
                maxlength: 40
            },
            j_newPassword: {
                required: true,
                minlength: 8,
                maxlength: 40,
                passwordPolicy: true
            },
            j_confirmNewPassword: {
                required: true,
                equalTo: "#j_newPassword",
                minlength: 5,
                maxlength: 40
            }
        },

        messages: {
            j_username: "Please enter email",
            j_oldPassword: "Please enter your current password",
            j_password: {
                required: "Please enter your new password",
                minlength: "Password must be 5-40 characters",
                maxlength: "Password must be 5-40 characters"
            },
            j_confirmPassword: {
                required: "Please confirm your new password",
                equalTo: "Password fields have to match",
                minlength: "Password must be 5-40 characters",
                maxlength: "Password must be 5-40 characters"
            }
        },

        submitHandler: function(form) {
            var $form = $(this);
            var postData = $(form).serializeArray();
            $.ajax({
                type: "POST",
                url: "/api/change-password",
                data: postData,
                success: function(data, textStatus, jqXHR) {
                    $("p#registerErrorMessage").text("A temporary password has been sent to your e-mail address");
                    $("p#registerErrorMessage").show();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                	if (jqXHR.status == 0) {
                		 $("p#registerErrorMessage").text("A temporary password has been sent to your e-mail address");
                         $("p#registerErrorMessage").show();
                		return;
                	}
                    $("p#registerErrorMessage").text(jqXHR.statusText);
                    $("p#registerErrorMessage").show();
                }
            });
        }
    });

    $forgotPasswordButton.on('click activate', function(e){
        var state = $(e.currentTarget).attr('data-state');
        stateController(state);

    });

    $stateControlButton.on('click activate', function(e){
        var state = $(e.currentTarget).attr('data-state');
        stateController(state);

    });

    $loginButton.on('click activate', function(e){
        $('#loginForm button[type="submit"]').trigger('click');
    });

    $newCustomer.on('activate', function(e){
        $('div#newCustomerContent.panel-collapse').collapse('show');

    });

    $returningCustomer.on('activate', function(e){
        $('div#returningCustomerContent.panel-collapse').collapse('show');
     });

    function stateController(new_state){
        state = new_state;
        switch(state) {
            case "forgot_password_invoke":
                $('#forgorPasswordFormDiv').removeClass('hidden').addClass('show');
                $('#loginFormDiv').removeClass('show').addClass('hidden');
                $forgotPasswordButton.text('Back to login').attr('data-state', 'login_invoke');
                $stateControlButton.text('Reset My Password').attr('data-state','forgot_password_call');
                break;
            case "forgot_password_call":
                $('#forgorPasswordForm button[type="submit"]').trigger('click');
                break;
            case "login_invoke":
                $('#forgorPasswordFormDiv').removeClass('show').addClass('hidden');
                $('#loginFormDiv').removeClass('hidden').addClass('show');
                $forgotPasswordButton.text('Forgot your password?').attr('data-state', 'forgot_password_invoke');
                $stateControlButton.text('Go!').attr('data-state','login_call');
                break;
            case "login_call":
                break;
            default:
                (function(){})();
        }
    }

    $('select').selectpicker({
        debug: true,
        style: 'btn-lg btn-default'
    });
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
        $('.selectpicker').selectpicker('mobile');
    }

    var query = (function(window, $){
        var tmpMap = location.search.substring(1,location.search.length).split("&"),
            paramMap = {},
            i;

        for(i=0; i < tmpMap.length; i++ ){
            var position = tmpMap[i].indexOf('='),
                key = tmpMap[i].substring(0, position),
                value = tmpMap[i].substring(position+1, tmpMap[i].length);

            paramMap[key] = value;
        }
        return paramMap;
    })(window, $, undefined);

    $('.panel-collapse').collapse({
        parent: "#accordion",
        toggle: false
    }).on('hidden.bs.collapse', function () {
        console.log('test');
    });

    if(query.display !== undefined){

        switch(query.display) {
            case "new":
                $newCustomer.trigger('activate');
                break;
            case "return":
                $returningCustomer.trigger('activate');
                break;
            case "forgot_password":
                $returningCustomer.trigger('activate');
                $forgotPasswordButton.trigger('activate');
                break;
        }
    }

});


