function getUrlParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

$(function() {
	var back = getUrlParameterByName('back');
	var close = getUrlParameterByName('close');
	var home = getUrlParameterByName('home');
	if (typeof (back) != 'undefined' && back != null) {
		$('a#back').attr('href', back);
	}
	if (typeof (close) != 'undefined' && close != null) {
		$('a#close').attr('href', close);
	}
	if (typeof (home) != 'undefined' && home != null) {
		$('a#siteUrl').attr('href', home);
	} else {
		$('a#siteUrl').attr('href', 'http://www.objectedge.com');
	}
});