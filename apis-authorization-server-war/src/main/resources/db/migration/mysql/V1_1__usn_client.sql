-- delete redirect URIs for USN client
delete from Client_redirectUris where client_id = (select id from client where clientId = 'usable_net_user_client');
-- delete scopes for USN client 
delete from Client_scopes where client_id = (select id from client where clientId = 'usable_net_user_client');
-- delete USN client
delete from client where clientId = 'usable_net_user_client';

-- delete redirect URIs for USN guest client
delete from Client_redirectUris where client_id = (select id from client where clientId = 'usable_net_guest_client');
-- delete scopes for USN guest client 
delete from Client_scopes where client_id = (select id from client where clientId = 'usable_net_guest_client');
-- delete USN guest client
delete from client where clientId = 'usable_net_guest_client';

-- insert USN client
INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, resourceserver_id, secret, skipConsent, useRefreshTokens, allowedImplicitGrant, allowedClientCredentials)
VALUES (89991, 'usable_net_user_client', 'dummy@yahoo.com', 'Dummy', 'UsableNet client to be used for registered users', 0,
    'usable_net_user_client', 99999 /* SSAPI Resource Server */, 'c0aea8f4-f606-4ebd-9d90-b70d324a3b84', 0, 0,  1, 0);
    
    -- insert USN guest client
INSERT INTO client (id, clientId, contactEmail, contactName, description, expireDuration, 
					clientName, resourceserver_id, secret, skipConsent, useRefreshTokens, allowedImplicitGrant, allowedClientCredentials)
VALUES (89992, 'usable_net_guest_client', 'dummy@yahoo.com', 'Dummy', 'UsableNet client to be used for registered users', 0,
    'usable_net_guest_client', 99999 /* SSAPI Resource Server */, '31b65db7-4fee-4e96-976f-4f6a56d3c706', 0, 0,  1, 0);
    
-- insert scopes for USN client
INSERT INTO Client_scopes values (89991, 'read');
INSERT INTO Client_scopes values (89991, 'write');
INSERT INTO Client_scopes values (89992, 'read');
INSERT INTO Client_scopes values (89992, 'write');

-- insert redirect URIs for USN client
INSERT INTO Client_redirectUris values (89991, 'https://m.surfstitch.com/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://m.surfstitch.com/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://surfstitch.upreus.com/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://surfstitch.upreus.com/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://uat-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://qa-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://alessandroa-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://marcop-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://davideb-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://lucai-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89991, 'https://michaelf-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89991, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89991, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89991, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89991, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

-- insert redirect URIs for USN guest client
INSERT INTO Client_redirectUris values (89992, 'https://m.surfstitch.com/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://m.surfstitch.com/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://m.surfstitch.com/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://surfstitch.upreus.com/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://surfstitch.upreus.com/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://surfstitch.upreus.com/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://uat-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://uat-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://qa-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://qa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://alessandroa-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://alessandroa-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://marcop-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://marcop-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://davideb-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://davideb-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://lucai-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://lucai-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');

INSERT INTO Client_redirectUris values (89992, 'https://michaelf-surfstitch-main.udev1a.net/h5/index/');
INSERT INTO Client_redirectUris values (89992, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true');
INSERT INTO Client_redirectUris values (89992, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=checkout');
INSERT INTO Client_redirectUris values (89992, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=favourites');
INSERT INTO Client_redirectUris values (89992, 'https://michaelf-surfstitch-main.udev1a.net/h5/user?fromAPI=true' || chr(38) || 'goto=account');