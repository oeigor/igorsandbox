/*
drop table [AbstractEntity];
drop table [AccessToken_scopes];
drop table [AuthorizationRequest_grantedScopes];
drop table [AuthorizationRequest_requestedScopes];
drop table [Client_redirectUris];
drop table [Client_scopes];
drop table [OPENJPA_SEQUENCE_TABLE];
drop table [ResourceServer_scopes];
drop table [accesstoken];
drop table [authorizationrequest];
drop table [client];
drop table [client_attributes];
drop table [resourceserver];
*/
CREATE TABLE [AbstractEntity] (
  [id] [int] NOT NULL,
  [creationDate] [datetime] DEFAULT NULL,
  [modificationDate] [datetime] DEFAULT NULL,
  [DTYPE] [varchar](255) DEFAULT NULL,
  PRIMARY KEY ([id]),
);  

CREATE INDEX I_BSTRTTY_DTYPE
    ON [AbstractEntity]
    (DTYPE); 

CREATE TABLE [AccessToken_scopes] (
  [ACCESSTOKEN_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL  
); 

CREATE INDEX I_CCSSCPS_ACCESSTOKEN_ID
    ON [AccessToken_scopes]
    (ACCESSTOKEN_ID); 

CREATE TABLE [AuthRequest_grantedScopes] (
  [AUTHORIZATIONREQUEST_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL  
);

CREATE INDEX I_THRZCPS_AUTHORIZATIONREQUEST_ID
    ON [AuthRequest_grantedScopes]
    (AUTHORIZATIONREQUEST_ID); 

CREATE TABLE [AuthRequest_requestedScopes] (
  [AUTHORIZATIONREQUEST_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL,  
); 

CREATE INDEX I_THRZCPS_AUTHORIZATIONREQUEST_ID1
    ON [AuthRequest_requestedScopes]
    (AUTHORIZATIONREQUEST_ID); 

CREATE TABLE [Client_redirectUris] (
  [CLIENT_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL  
);

CREATE INDEX I_CLNTTRS_CLIENT_ID
    ON [Client_redirectUris]
    (CLIENT_ID); 

CREATE TABLE [Client_scopes] (
  [CLIENT_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL
);  

CREATE INDEX I_CLNTCPS_CLIENT_ID
    ON [Client_scopes]
    (CLIENT_ID); 

CREATE TABLE [OPENJPA_SEQUENCE_TABLE] (
  [ID] [int] NOT NULL,
  [SEQUENCE_VALUE] [int] DEFAULT NULL,
  PRIMARY KEY ([ID])
);

CREATE TABLE [ResourceServer_scopes] (
  [RESOURCESERVER_ID] [int] DEFAULT NULL,
  [element] [varchar](255) DEFAULT NULL  
);

CREATE INDEX I_RSRCCPS_RESOURCESERVER_ID
    ON [ResourceServer_scopes]
    (RESOURCESERVER_ID); 

CREATE TABLE [accesstoken] (
  [id] [int] NOT NULL,
  [creationDate] datetime DEFAULT NULL,
  [modificationDate] datetime DEFAULT NULL,
  [encodedPrincipal] TEXT DEFAULT NULL,
  [expires] [bigint] DEFAULT NULL,
  [refreshToken] [varchar](255) DEFAULT NULL,
  [resourceOwnerId] [varchar](255) DEFAULT NULL,
  [token] [varchar](255) DEFAULT NULL,
  [client_id] [int] NOT NULL,
  PRIMARY KEY ([id]),
  CONSTRAINT U_CCSSTKN_TOKEN UNIQUE (token)  
);

CREATE INDEX I_CCSSTKN_CLIENT
    ON [accesstoken]
    (client_id); 

CREATE TABLE [authorizationrequest] (
  [id] [int] NOT NULL,
  [creationDate] datetime DEFAULT NULL,
  [modificationDate] datetime DEFAULT NULL,
  [authState] [varchar](255) DEFAULT NULL,
  [authorizationCode] [varchar](255) DEFAULT NULL,
  [encodedPrincipal] TEXT DEFAULT NULL,
  [redirectUri] [varchar](255) DEFAULT NULL,
  [responseType] [varchar](255) DEFAULT NULL,
  [state] [varchar](255) DEFAULT NULL,
  [client_id] [int] NOT NULL,
  PRIMARY KEY ([id])  
); 

CREATE INDEX [I_THRZQST_CLIENT]
    ON [authorizationrequest]
    (client_id); 

CREATE TABLE [client] (
  [id] [int] NOT NULL,
  [creationDate] datetime DEFAULT NULL,
  [modificationDate] datetime DEFAULT NULL,
  [clientId] [varchar](255) DEFAULT NULL,
  [contactEmail] [varchar](255) DEFAULT NULL,
  [contactName] [varchar](255) DEFAULT NULL,
  [description] [varchar](255) DEFAULT NULL,
  [expireDuration] [int] DEFAULT NULL,
  [clientName] [varchar](255) DEFAULT NULL,
  [allowedImplicitGrant] bit DEFAULT NULL,
  [allowedClientCredentials] bit DEFAULT NULL,
  [secret] [varchar](255) DEFAULT NULL,
  [skipConsent] bit DEFAULT NULL,
  [includePrincipal] bit DEFAULT NULL,
  [thumbNailUrl] [varchar](255) DEFAULT NULL,
  [useRefreshTokens] bit DEFAULT NULL,
  [resourceserver_id] [int] NOT NULL,
  PRIMARY KEY ([id]),
  CONSTRAINT [U_CLIENT_CLIENTID] UNIQUE ([clientId]),  
);

CREATE INDEX [I_CLIENT_RESOURCESERVER]
    ON [client]
    (resourceserver_id); 

CREATE TABLE [client_attributes] (
  [client_id] [int] DEFAULT NULL,
  [attribute_name] [varchar](255) NOT NULL,
  [attribute_value] [varchar](255) DEFAULT NULL  
);

CREATE INDEX [I_CLNTBTS_CLIENT_ID]
    ON [client_attributes]
    ([client_id]); 

CREATE TABLE [resourceserver] (
  [id] [int] NOT NULL,
  [creationDate] datetime DEFAULT NULL,
  [modificationDate] datetime DEFAULT NULL,
  [contactEmail] [varchar](255) DEFAULT NULL,
  [contactName] [varchar](255) NOT NULL,
  [description] [varchar](255) DEFAULT NULL,
  [resourceServerKey] [varchar](255) DEFAULT NULL,
  [resourceServerName] [varchar](255) DEFAULT NULL,
  [owner] [varchar](255) DEFAULT NULL,
  [secret] [varchar](255) NOT NULL,
  [thumbNailUrl] [varchar](255) DEFAULT NULL,
  PRIMARY KEY ([id]),
  CONSTRAINT [U_RSRCRVR_CONSTRAINT] UNIQUE ([resourceServerKey]),
  CONSTRAINT [U_RSRCRVR_OWNER] UNIQUE ([owner], [resourceServerName])
);
