package com.objectedge.oaaas.authentication;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.surfnet.oaaas.auth.AbstractAuthenticator;
import org.surfnet.oaaas.auth.AbstractFilter;
import org.surfnet.oaaas.auth.OAuth2Validator;
import org.surfnet.oaaas.model.AuthorizationRequest;
import org.surfnet.oaaas.repository.AuthorizationRequestRepository;

import com.objectedge.api.OEAPIClient;

@PropertySource("classpath:apis.application.properties")
public abstract class OEAbstractFilter extends AbstractFilter {

	@Inject
	Environment env;

	@Inject
	private OEAPIClient apiClient;

	@Inject
	private AuthorizationRequestRepository authorizationRequestRepository;

	@Inject
	private OAuth2Validator oAuth2Validator;

	protected AuthorizationRequest extractAuthorizationRequest(HttpServletRequest request) {
		String responseType = request.getParameter("response_type");
		String clientId = request.getParameter("client_id");
		String redirectUri = request.getParameter("redirect_uri");

		List<String> requestedScopes = null;
		if (StringUtils.isNotBlank(request.getParameter("scope"))) {
			requestedScopes = Arrays.asList(request.getParameter("scope").split(","));
		}

		String state = request.getParameter("state");
		String authState = getAuthStateValue();

		return new AuthorizationRequest(responseType, clientId, redirectUri, requestedScopes, state, authState);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {

	}
	
	protected AuthorizationRequest findAuthorizationRequest(HttpServletRequest request) {
		String authState = (String) request.getAttribute(AbstractAuthenticator.AUTH_STATE);
		if (StringUtils.isBlank(authState)) {
			authState = request.getParameter(AbstractAuthenticator.AUTH_STATE);
		}
		return getAuthorizationRequestRepository().findByAuthState(authState);
	}

	protected boolean initialRequest(HttpServletRequest request) {
		return request.getParameter(AUTH_STATE)==null;
	}

	protected String getAuthStateValue() {
		return UUID.randomUUID().toString();
	}

	public OAuth2Validator getoAuth2Validator() {
		return oAuth2Validator;
	}

	public void setoAuth2Validator(OAuth2Validator oAuth2Validator) {
		this.oAuth2Validator = oAuth2Validator;
	}

	public AuthorizationRequestRepository getAuthorizationRequestRepository() {
		return authorizationRequestRepository;
	}

	public void setAuthorizationRequestRepository(
			AuthorizationRequestRepository authorizationRequestRepository) {
		this.authorizationRequestRepository = authorizationRequestRepository;
	}

	public OEAPIClient getApiClient() {
		return apiClient;
	}

	public void setApiClient(OEAPIClient apiClient) {
		this.apiClient = apiClient;
	}

	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

}
