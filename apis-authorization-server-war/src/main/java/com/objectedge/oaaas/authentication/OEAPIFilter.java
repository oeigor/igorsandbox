package com.objectedge.oaaas.authentication;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.surfnet.oaaas.auth.AbstractAuthenticator;
import org.surfnet.oaaas.auth.AbstractUserConsentHandler;
import org.surfnet.oaaas.auth.OAuth2Validator.ValidationResponse;
import org.surfnet.oaaas.model.AuthorizationRequest;

import com.surfstitch.oaaas.authentication.SSAPIFilter;

public class OEAPIFilter extends OEAbstractFilter {

	private static final Logger LOG = LoggerFactory.getLogger(OEAPIFilter.class);
	private static final String URI_CHANGE_PASSWORD = "/api/change-password";
	private static final String ENV_PROP_URL_CHANGE_PASSWORD = "change_password_url";

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		try {
			HttpServletRequest request = (HttpServletRequest) req;
			HttpServletResponse response = (HttpServletResponse) res;

			boolean initialRequest = initialRequest(request);

			AuthorizationRequest authorizationRequest = null;
			if(initialRequest) {
				authorizationRequest = extractAuthorizationRequest(request);
				final ValidationResponse validationResponse = getoAuth2Validator().validate(authorizationRequest);
				if(!validationResponse.valid()){
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Auth request not valid on the Request");
				}
				getAuthorizationRequestRepository().save(authorizationRequest);
				request.setAttribute(AbstractAuthenticator.RETURN_URI, getReturnUri(request));
				request.setAttribute(AbstractUserConsentHandler.CLIENT, authorizationRequest.getClient());
				request.setAttribute(AUTH_STATE, authorizationRequest.getAuthState());
				request.setAttribute("actionUri", request.getRequestURI());
				((HttpServletResponse) response).setHeader("X-Frame-Options", "SAMEORIGIN");
				
				if(URI_CHANGE_PASSWORD.equalsIgnoreCase(request.getRequestURI())) {
					request.getRequestDispatcher("/WEB-INF/jsp/surfstitch/change_password.jsp").forward(request, response);
				}
			} else {
				authorizationRequest = findAuthorizationRequest(request);				
				if (authorizationRequest == null) {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No valid AbstractAuthenticator.AUTH_STATE on the Request");
					return;
				}
				JSONObject responseJSON = null;
				if(request.getRequestURI().equals("/api/change-password")){
					responseJSON = getApiClient().handleChangePassword((HttpServletRequest)req, getEnv().getProperty(ENV_PROP_URL_CHANGE_PASSWORD));
					if (responseJSON.optString("updateStatus") != null) {
						if (responseJSON.optString("updateStatus").equalsIgnoreCase("200")) {
							response.setHeader("redirect", authorizationRequest.getRedirectUri());
						} else if (responseJSON.optString("updateStatus").equalsIgnoreCase("101")){
							response.sendError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Error during change password.");
						} else {
							response.sendError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Unable to read response from SSAPI server.");
						}
					} else {
						response.sendError(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Unable to read response from SSAPI server.");
					}
				} else {
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid request");
				}				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
