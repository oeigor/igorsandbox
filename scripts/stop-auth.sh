
if [ `ps -ef | grep "jetty" | grep -v grep | wc -l` = 1 ]; then
   echo "Stopping OE AuthServer"
   cd ../apis-authorization-server-war
   nohup mvn jetty:stop > /tmp/authServerConsole.out &
else
   echo "OE AuthServer is not running"
fi
