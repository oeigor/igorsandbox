if [ `ps -ef | grep "jetty" | grep -v grep | wc -l` = 0 ]; then
   echo "Starting OE AuthServer"
   cd ../apis-authorization-server-war
   nohup mvn jetty:run -Djetty.port=5024 > /tmp/authServerConsole.out &
else
   echo "OE AuthServer is already running. run ps -ef | grep jetty"
fi
